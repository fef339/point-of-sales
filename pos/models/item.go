package models

import "github.com/jinzhu/gorm"

// Item is item model of table
type Item struct {
	gorm.Model

	Name        string
	Description string
	BarcodeID   string `gorm:"unique;not null"`
	Price       int64
}
