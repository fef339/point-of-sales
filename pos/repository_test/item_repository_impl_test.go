package repository_test

import (
	"github.com/jinzhu/gorm" //nolint:goimports
	"github.com/stretchr/testify/assert"
	"log"
	"pos/pos/models"
	"pos/pos/repository"
	"pos/utils/db"
	"testing"
)

var itemRepository repository.ItemRepository

func init() {
	database := db.NewDatabase(db.SQLliteStorage)
	sqliteDB, err := database.Connect("file::memory:?cache=shared")
	if err != nil {
		log.Fatalf("Error Connecting Database %v\n", err)
	}
	sqliteDB.AutoMigrate(&models.Item{})
	itemRepository = repository.NewItemRepository(sqliteDB)
}

func TestSaveItem(t *testing.T) {
	newItem := &models.Item{
		BarcodeID:   "12345",
		Name:        "Es Americano",
		Description: "Espresso with mineral water",
		Price:       5000,
	}

	savedItem, err := itemRepository.Save(newItem)

	if err != nil {
		t.Errorf("this is error saving items: %v\n", err)
		return
	}

	assert.Equal(t, newItem.BarcodeID, savedItem.BarcodeID)
}

func TestSaveItemBadCase(t *testing.T) {
	newItem := &models.Item{
		BarcodeID:   "12345",
		Name:        "Es Americano",
		Description: "Espresso with mineral water",
		Price:       5000,
	}

	_, err := itemRepository.Save(newItem)

	assert.NotNil(t, err)
}

func TestUpdateItem(t *testing.T) {
	updateItem := &models.Item{
		Model:       gorm.Model{ID: 1},
		BarcodeID:   "12345",
		Name:        "Es Coffee Latte",
		Description: "Coffee With Latte",
		Price:       7000,
	}

	savedItem, err := itemRepository.Update(updateItem)
	if err != nil {
		t.Errorf("this is error update items: %v\n", err)
		return
	}

	assert.Equal(t, updateItem.Price, savedItem.Price)
}

func TestGetByBarcodeID(t *testing.T) {
	item, err := itemRepository.GetByBarcodeID("12345")
	if err != nil {
		t.Errorf("this is error GetByBarcodeID: %v\n", err)
		return
	}

	assert.Equal(t, "Es Coffee Latte", item.Name)
}

func TestGetByBarcodeIDBadCase(t *testing.T) {
	_, err := itemRepository.GetByBarcodeID("123456")

	assert.NotNil(t, err)
}

func TestDeleteItem(t *testing.T) {
	rowAffected, err := itemRepository.Delete(1)
	if err != nil {
		t.Errorf("this is error Delete: %v\n", err)
		return
	}

	assert.Equal(t, rowAffected, int64(1))
}
