package repository

import (
	"pos/pos/models"

	"github.com/jinzhu/gorm"
)

type itemRepositoryImpl struct {
	DB *gorm.DB
}

// NewItemRepository to declare the repository
func NewItemRepository(db *gorm.DB) ItemRepository {
	return &itemRepositoryImpl{db}
}

// GetByBarcodeID
func (i *itemRepositoryImpl) GetByBarcodeID(barcodeID string) (*models.Item, error) {
	var n = &models.Item{}

	if err := i.DB.First(&n, "barcode_id = ?", barcodeID).Error; err != nil {
		return n, err
	}

	return n, nil
}

func (i *itemRepositoryImpl) Save(items *models.Item) (*models.Item, error) {
	if err := i.DB.Create(items).Error; err != nil {
		return items, err
	}
	return items, nil
}

func (i *itemRepositoryImpl) Update(items *models.Item) (*models.Item, error) {
	if err := i.DB.Model(&items).Update(items).Error; err != nil {
		return items, err
	}
	return items, nil
}

func (i *itemRepositoryImpl) Delete(id int64) (int64, error) {

	db := i.DB.Delete(models.Item{}, "id = ?", id)

	if err := db.Error; err != nil {
		return 0, err
	}

	return db.RowsAffected, nil
}
