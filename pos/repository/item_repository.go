package repository

import "pos/pos/models"

// ItemRepository is Interface for Repository Item
type ItemRepository interface {
	GetByBarcodeID(barcodeID string) (*models.Item, error)
	Save(items *models.Item) (*models.Item, error)
	Update(items *models.Item) (*models.Item, error)
	Delete(id int64) (int64, error)
}
