package db

import "github.com/jinzhu/gorm"

// Database is interface to database purpose
type Database interface {
	Connect(url string) (*gorm.DB, error)
}
