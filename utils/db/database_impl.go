package db

import (
	"github.com/jinzhu/gorm"
	// Database Driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// DatabaseType is type of the database that we use(MySQL, PostgresSQL, SQLlite)
type DatabaseType int

const (
	//MySQLStorage is constant for MySQL
	MySQLStorage DatabaseType = 1 << iota

	//PostgreSQLStorage is constant for PostgreSQL
	PostgreSQLStorage

	//SQLliteStorage is constant for SQLlite
	SQLliteStorage
)

// NewDatabase used for create Database Factory
func NewDatabase(t DatabaseType) Database {
	switch t {
	case MySQLStorage:
		return newMySQLStorage()
	case PostgreSQLStorage:
		return newPostgreSQLStorage()
	default:
		return newSqlliteStorage()
	}
}

// Initialize for MySQL

type mySQLStorage struct {
}

func newMySQLStorage() Database {
	return &mySQLStorage{}
}

func (m *mySQLStorage) Connect(url string) (*gorm.DB, error) {
	return gorm.Open("mysql", url)
}

// Initialize for PostgreSQL

type postgreSQLStorage struct {
}

func newPostgreSQLStorage() Database {
	return &postgreSQLStorage{}
}

func (p *postgreSQLStorage) Connect(url string) (*gorm.DB, error) {
	return gorm.Open("postgres", url)
}

// Initialize for Sqllite3

type sqllite3Storage struct {
}

func newSqlliteStorage() Database {
	return &sqllite3Storage{}
}

func (s *sqllite3Storage) Connect(url string) (*gorm.DB, error) {
	return gorm.Open("sqlite3", url)
}
